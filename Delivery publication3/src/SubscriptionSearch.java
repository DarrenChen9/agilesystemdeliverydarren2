import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.border.BevelBorder;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.ActionEvent;
import javax.swing.JRadioButton;

public class SubscriptionSearch {
	private ButtonGroup productionGroup = new ButtonGroup();
	JFrame frame;
	private JTextField textField;
	private Database dbase = null;
	private Subscription sc = null;
	private String newspaper = null;

	/**
	 * Launch the application.
	 */

	/**
	 * Create the application.
	 */
	public SubscriptionSearch(String s, Database d, Subscription sub) {
		super();
		dbase = d;
		initialize();
		sc = sub;
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 538, 465);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setForeground(Color.BLACK);
		panel.setBorder(BorderFactory.createTitledBorder("SearchByID"));
		panel.setBackground(Color.LIGHT_GRAY);
		panel.setBounds(46, 13, 435, 124);
		frame.getContentPane().add(panel);

		JLabel lblId = new JLabel("ID:                 ");
		lblId.setBounds(6, 23, 233, 26);
		panel.add(lblId);

		textField = new JTextField(10);
		textField.setBounds(6, 49, 233, 26);
		panel.add(textField);

		JButton button = new JButton("Search");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		button.setBounds(249, 49, 90, 27);
		panel.add(button);

		JPanel panel_1 = new JPanel();
		panel_1.setLayout(null);
		panel_1.setForeground(Color.BLACK);
		panel_1.setBorder(BorderFactory.createTitledBorder("SearchByNewspaper"));
		panel_1.setBackground(Color.LIGHT_GRAY);
		panel_1.setBounds(46, 165, 435, 221);
		frame.getContentPane().add(panel_1);

		JButton btnSearch = new JButton("Search");
		btnSearch.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btnSearch.setBounds(6, 174, 175, 27);
		panel_1.add(btnSearch);

		JButton button_2 = new JButton("Cancel");
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// Code to open the main menu
				MainMenu main = new MainMenu();
				main.frame.setVisible(true);
				frame.setVisible(false);
				frame.dispose();
			}
		});
		button_2.setFont(new Font("Tahoma", Font.PLAIN, 18));
		button_2.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		button_2.setBounds(223, 174, 182, 27);
		panel_1.add(button_2);

		JRadioButton rdbtnNewRadioButton = new JRadioButton("Irish Independent");
		rdbtnNewRadioButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		rdbtnNewRadioButton.setBounds(10, 43, 171, 27);
		panel_1.add(rdbtnNewRadioButton);

		JRadioButton rdbtnNewRadioButton_1 = new JRadioButton("Irish Mirror");
		rdbtnNewRadioButton_1.setBounds(223, 43, 182, 27);
		panel_1.add(rdbtnNewRadioButton_1);

		JRadioButton rdbtnLongfordLeader = new JRadioButton("Longford Leader");
		rdbtnLongfordLeader.setBounds(6, 107, 175, 27);
		panel_1.add(rdbtnLongfordLeader);

		JRadioButton rdbtnNewRadioButton_2 = new JRadioButton("Westmeath Topic");
		rdbtnNewRadioButton_2.setBounds(223, 107, 182, 27);
		panel_1.add(rdbtnNewRadioButton_2);

		productionGroup.add(rdbtnNewRadioButton);
		productionGroup.add(rdbtnNewRadioButton_1);
		productionGroup.add(rdbtnLongfordLeader);
		productionGroup.add(rdbtnNewRadioButton_2);

		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (rdbtnNewRadioButton.isSelected()) {
					newspaper = "Independent";
				} else if (rdbtnNewRadioButton_1.isSelected()) {
					newspaper = "Mirror";
				} else if (rdbtnLongfordLeader.isSelected()) {
					newspaper = "Leader";
				} else if (rdbtnNewRadioButton_2.isSelected()) {
					newspaper = "Topic";
				}

			}
		});
		myEvent();
	}

	public void myEvent() {
		frame.addWindowListener(new WindowAdapter() {

			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});

		textField.addKeyListener(new KeyAdapter() {

			public void keyTyped(KeyEvent e) {
				int keyChar = e.getKeyChar();  
				if ((keyChar > KeyEvent.VK_0 && keyChar < KeyEvent.VK_9)
						|| keyChar == KeyEvent.VK_BACK_SPACE) {
					frame.setVisible(true);
				} else {
					e.consume();
					JOptionPane.showMessageDialog(null, "Number Only!", "Error",1);
					frame.setVisible(true);
				}
			}
		});
	}
}
