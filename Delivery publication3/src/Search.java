import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import java.awt.Component;
import javax.swing.ScrollPaneConstants;
import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.event.AncestorListener;
import javax.swing.event.AncestorEvent;
import javax.swing.JPanel;
import java.awt.GridLayout;
import java.awt.Label;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.border.BevelBorder;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import java.awt.event.ActionEvent;

public class Search {

	JFrame frame;
	private JTextField IdText;
	private JTextField FirstNameText;
	private JTextField SurNameText;
	private Database dbase = null;
	private Customers c= null;
	private Label label;

	/**
	 * Launch the application.
	 */


	/**
	 * Create the application.
	 */
	public Search(String s,Database d,Customers cust) {
		super();
		dbase=d;
		c=cust;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 603, 472);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane((Component) null, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.addAncestorListener(new AncestorListener() {
			public void ancestorAdded(AncestorEvent arg0) {
			}
			public void ancestorMoved(AncestorEvent arg0) {
			}
			public void ancestorRemoved(AncestorEvent arg0) {
			}
		});
		scrollPane.setToolTipText("");
		scrollPane.setBorder(BorderFactory.createTitledBorder("SearchByID"));
		scrollPane.setBackground(Color.LIGHT_GRAY);
		scrollPane.setBounds(273, 58, 303, 146);
		frame.getContentPane().add(scrollPane);
		
		JScrollPane scrollPane_1 = new JScrollPane((Component) null, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane_1.setBorder(BorderFactory.createTitledBorder("SearchByName"));
		scrollPane_1.setBackground(Color.LIGHT_GRAY);
		scrollPane_1.setBounds(273, 233, 303, 146);
		frame.getContentPane().add(scrollPane_1);
		
		JPanel panel = new JPanel();
		panel.addAncestorListener(new AncestorListener() {
			public void ancestorAdded(AncestorEvent event) {
			}
			public void ancestorMoved(AncestorEvent event) {
			}
			public void ancestorRemoved(AncestorEvent event) {
			}
		});
		panel.setForeground(Color.BLACK);
		panel.setBorder(BorderFactory.createTitledBorder( "Search"));
		panel.setBackground(Color.LIGHT_GRAY);
		panel.setBounds(14, 59, 245, 320);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel lblSearchbyid = new JLabel("SearchByID:                 ");
		lblSearchbyid.setBounds(6, 23, 233, 26);
		panel.add(lblSearchbyid);
		
		IdText = new JTextField(10);
		IdText.setBounds(6, 49, 233, 26);
		panel.add(IdText);
		
		JLabel lblSearchbyname = new JLabel("SearchByName:               ");
		lblSearchbyname.setBounds(6, 155, 233, 26);
		panel.add(lblSearchbyname);
		
		FirstNameText = new JTextField(10);
		FirstNameText.setBounds(92, 194, 147, 26);
		panel.add(FirstNameText);
		
		JButton btnSearch = new JButton("Search");
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				c=new Customers("ss","ss","ss","ss",dbase);
				try {
					c.searchCustomerID(Integer.parseInt(IdText.getText()));
				} catch (NumberFormatException | SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		});
		btnSearch.setBounds(6, 115, 113, 27);
		panel.add(btnSearch);
		
		JButton button = new JButton("Search");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				c=new Customers("ss","ss","ss","ss",dbase);
				try {
					c.searchCustomerName(FirstNameText.getText(), SurNameText.getText());
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
		});
		button.setBounds(4, 279, 113, 27);
		panel.add(button);
		
		SurNameText = new JTextField(10);
		SurNameText.setBounds(92, 239, 147, 26);
		panel.add(SurNameText);
		
		JLabel lblFirstname = new JLabel("FirstName");
		lblFirstname.setBounds(6, 198, 72, 18);
		panel.add(lblFirstname);
		
		JLabel lblSurname = new JLabel("SurName");
		lblSurname.setBounds(6, 243, 72, 18);
		panel.add(lblSurname);
		
		JLabel lblSearch = new JLabel("Search");
		lblSearch.setFont(new Font("����", Font.BOLD, 20));
		lblSearch.setBounds(224, 1, 116, 62);
		frame.getContentPane().add(lblSearch);
		
		JButton button_1 = new JButton("Cancel");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//Code to open the main menu			
				MainMenu main = new MainMenu();
				main.frame.setVisible(true);
				frame.setVisible(false); 
				frame.dispose(); 
			}
		});
		button_1.setFont(new Font("Tahoma", Font.PLAIN, 18));
		button_1.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		button_1.setBounds(188, 385, 152, 27);
		frame.getContentPane().add(button_1);
		
		myEvent();
	}

	public void myEvent() {
		frame.addWindowListener(new WindowAdapter() {

			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});

		IdText.addKeyListener(new KeyAdapter() {

			public void keyTyped(KeyEvent e) {
				int keyChar = e.getKeyChar();  
				if ((keyChar > KeyEvent.VK_0 && keyChar < KeyEvent.VK_9)
						|| keyChar == KeyEvent.VK_BACK_SPACE) {
					frame.setVisible(true);
				} else {
					e.consume();
					JOptionPane.showMessageDialog(null, "Number Only!", "Error",1);
					frame.setVisible(true);
				}
			}
		});
		FirstNameText.addKeyListener(new KeyAdapter() {

			public void keyTyped(KeyEvent e) {
				  char c = e.getKeyChar();
				    if(!Character.isLetter(c) || 127<c)
				    {
				    	e.consume();
				    JOptionPane.showMessageDialog(null, "Alphabet Only!", "Error",1);				    
				    }
				    else {
				    	frame.setVisible(true);
				    }
				    }
		});
		SurNameText.addKeyListener(new KeyAdapter() {

			public void keyTyped(KeyEvent e) {
				  char c = e.getKeyChar();
				    if(!Character.isLetter(c) || 127<c)
				    {
				    	e.consume();
				    JOptionPane.showMessageDialog(null, "Alphabet Only!", "Error",1);				    
				    }
				    else {
				    	frame.setVisible(true);
				    }
				    }
		});
	}
}
