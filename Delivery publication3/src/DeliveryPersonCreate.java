import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class DeliveryPersonCreate {

	JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private Customers[] Customers;
	private Database dbase = null;
	private DeliveryPerson dp = null;
	private JButton btnCancel;
	/**
	 * Launch the application.
	 */


	/**
	 * Create the application.
	 */
	public DeliveryPersonCreate(String s ,Database d,DeliveryPerson del) {
		super();
		initialize();
		dbase=d;
		dp=del;
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 487, 435);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblDeliveryPersonInformation = new JLabel("Delivery Person Information");
		lblDeliveryPersonInformation.setFont(new Font("����", Font.BOLD, 20));
		lblDeliveryPersonInformation.setBounds(85, 13, 297, 38);
		frame.getContentPane().add(lblDeliveryPersonInformation);
		
		JLabel lblFirstname = new JLabel("FirstName");
		lblFirstname.setFont(new Font("����", Font.BOLD, 18));
		lblFirstname.setBounds(37, 117, 104, 18);
		frame.getContentPane().add(lblFirstname);
		
		JLabel lblLastname = new JLabel("LastName");
		lblLastname.setFont(new Font("����", Font.BOLD, 18));
		lblLastname.setBounds(37, 193, 104, 18);
		frame.getContentPane().add(lblLastname);
		
		JLabel lblDeliveryArea = new JLabel("Delivery Area");
		lblDeliveryArea.setFont(new Font("����", Font.BOLD, 18));
		lblDeliveryArea.setBounds(37, 269, 135, 21);
		frame.getContentPane().add(lblDeliveryArea);
		
		textField = new JTextField();
		textField.setBounds(201, 116, 168, 24);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(201, 192, 168, 24);
		frame.getContentPane().add(textField_1);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(201, 269, 168, 24);
		frame.getContentPane().add(textField_2);
		
		JButton btnNewButton = new JButton("Create Delivery Person");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				dp = new DeliveryPerson(textField.getText(), textField_1.getText(), textField_2.getText(), dbase);
				boolean res = false;

				res = dp.insertDeliveryPerson();

				if (res == true) {
					JOptionPane.showMessageDialog(null, "Insert Successful");
				} else {
					JOptionPane.showMessageDialog(null, "Insert Failed");
				}

			}
		});
		btnNewButton.setBounds(14, 331, 220, 44);
		frame.getContentPane().add(btnNewButton);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Code to open the main menu
				MainMenu main = new MainMenu();
				main.frame.setVisible(true);
				frame.setVisible(false);
				frame.dispose();
			}
		});
		btnCancel.setBounds(287, 331, 168, 44);
		frame.getContentPane().add(btnCancel);
		myEvent();
	}

	private void myEvent() {
		textField.addKeyListener(new KeyAdapter() {

			public void keyTyped(KeyEvent e) {
				  char c = e.getKeyChar();
				    if(!Character.isLetter(c) || 127<c)
				    {
				    	e.consume();
				    JOptionPane.showMessageDialog(null, "Alphabet Only!", "Error",1);				    
				    }
				    else {
				    	frame.setVisible(true);
				    }
				    }
		});
		textField_1.addKeyListener(new KeyAdapter() {

			public void keyTyped(KeyEvent e) {
				  char c = e.getKeyChar();
				    if(!Character.isLetter(c) || 127<c)
				    {
				    	e.consume();
				    JOptionPane.showMessageDialog(null, "Alphabet Only!", "Error",1);				    
				    }
				    else {
				    	frame.setVisible(true);
				    }
				    }
		});
	}
}
