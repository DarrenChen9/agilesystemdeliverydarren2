import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import java.sql.SQLException;
import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CreateNewAccount {
	JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;

	private Customers[] Customers;
	private Database dbase = null;
	private Customers c= null;
	private Subscription Subscription;

	/**
	 * Launch the application.
	 */

	/**
	 * Create the application.
	 */
	public CreateNewAccount(String s, Database d, Customers cust)
	{
		super();
		dbase = d;
		c  = cust;initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 543, 501);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblAccountInformation = new JLabel("Account Information");
		lblAccountInformation.setFont(new Font("宋体", Font.BOLD, 20));
		lblAccountInformation.setBounds(154, 30, 231, 40);
		frame.getContentPane().add(lblAccountInformation);
		
		JLabel lblFirstName = new JLabel("First Name");
		lblFirstName.setFont(new Font("宋体", Font.BOLD, 18));
		lblFirstName.setBounds(43, 118, 120, 18);
		frame.getContentPane().add(lblFirstName);
		
		JLabel lblLastName = new JLabel("Last Name");
		lblLastName.setFont(new Font("宋体", Font.BOLD, 18));
		lblLastName.setBounds(43, 187, 120, 18);
		frame.getContentPane().add(lblLastName);
		
		JLabel lblAddress = new JLabel("Address");
		lblAddress.setFont(new Font("宋体", Font.BOLD, 18));
		lblAddress.setBounds(43, 265, 120, 18);
		frame.getContentPane().add(lblAddress);
		
		JLabel lblPhoneNumber = new JLabel("Phone Number");
		lblPhoneNumber.setFont(new Font("宋体", Font.BOLD, 18));
		lblPhoneNumber.setBounds(43, 345, 120, 18);
		frame.getContentPane().add(lblPhoneNumber);
		
		textField = new JTextField();
		textField.setBounds(255, 117, 181, 24);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(255, 186, 181, 24);
		frame.getContentPane().add(textField_1);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(255, 264, 181, 24);
		frame.getContentPane().add(textField_2);
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(255, 344, 181, 24);
		frame.getContentPane().add(textField_3);
		
		JButton btnNewButton = new JButton("Create Account");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String PhoneNumber = textField_3.getText();
				String FirstName=textField.getText();
				String LastName=textField_1.getText();
				String Address=textField_2.getText();
				String SUBS = PhoneNumber.substring(0,2);
				c =new Customers(FirstName,LastName,Address,PhoneNumber,dbase);
				boolean res = false;
				try 
				{
					res = c.insertCustomer();
				}
				catch (SQLException e1) 
				{
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				if(res == true)
				{
					JOptionPane.showMessageDialog(null, "Insert Successful");
				    SubscribeService sub=new SubscribeService("Create Subscribe Service",Subscription, dbase);
					
				    frame.setVisible(false); 
					frame.dispose(); 

				}
				else
				{
					JOptionPane.showMessageDialog(null, "Insert Failed");
				}
			}
			
			
		});
		btnNewButton.setFont(new Font("宋体", Font.BOLD, 18));
		btnNewButton.setBounds(29, 401, 188, 40);
		frame.getContentPane().add(btnNewButton);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				//Code to open the main menu			
				MainMenu main = new MainMenu();
				main.frame.setVisible(true);
				frame.setVisible(false); 
				frame.dispose(); 
			}
		});
		btnCancel.setFont(new Font("宋体", Font.BOLD, 18));
		btnCancel.setBounds(272, 401, 181, 40);
		frame.getContentPane().add(btnCancel);
		myEvent();
	}

	public void myEvent() {
		textField.addKeyListener(new KeyAdapter() {

			public void keyTyped(KeyEvent e) {
				  char c = e.getKeyChar();
				    if(!Character.isLetter(c) || 127<c)
				    {
				    	e.consume();
				    JOptionPane.showMessageDialog(null, "Alphabet Only!", "Error",1);				    
				    }
				    else {
				    	frame.setVisible(true);
				    }
				    }
		});
		textField_1.addKeyListener(new KeyAdapter() {

			public void keyTyped(KeyEvent e) {
				  char c = e.getKeyChar();
				    if(!Character.isLetter(c) || 127<c)
				    {
				    	e.consume();
				    JOptionPane.showMessageDialog(null, "Alphabet Only!", "Error",1);				    
				    }
				    else {
				    	frame.setVisible(true);
				    }
				    }
		});
		textField_3.addKeyListener(new KeyAdapter() {

			public void keyTyped(KeyEvent e) {
				int keyChar = e.getKeyChar();  
				if ((keyChar > KeyEvent.VK_0 && keyChar < KeyEvent.VK_9)
						|| keyChar == KeyEvent.VK_BACK_SPACE) {
					frame.setVisible(true);
				} else {
					e.consume();
					JOptionPane.showMessageDialog(null, "Number Only!", "Error",1);
					frame.setVisible(true);
				}
			}
		});
	}

}
